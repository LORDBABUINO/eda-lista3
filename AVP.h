#include<No.h>
#include<stdio.h>

#define PRETA false
#define VERMELHA true

class AVP{

	private:

		No *raiz = NULL;

		void rotacionar_esquerda(No *pai){

			if(pai == NULL)
				return;
			if(pai->getRight() == NULL)
				return;

			No* filho = pai->getRight();
			No* esquerdaDoFilho = filho->getLeft();
			No* avo = pai->getPai();

			if(avo != NULL){
				if(avo->getRight() == pai)
					avo->setRight(filho);
				else
					avo->setLeft(filho);
			}

			filho->setPai(pai->getPai());
			filho->setLeft(pai);

			pai->setPai(filho);
			pai->setRight(esquerdaDoFilho);

			if(esquerdaDoFilho != NULL)
				esquerdaDoFilho->setPai(pai);

			if(pai == raiz)
				raiz = filho;
		}
                void rotacionar_direita(No *pai){

                        if(pai == NULL)
				return;
			if(pai->getLeft() == NULL)
                                return;

                        No* filho = pai->getLeft();
                        No* direitaDoFilho = filho->getRight();
                        No* avo = pai->getPai();

                       if(avo != NULL){
                                if(avo->getRight() == pai)
                                        avo->setRight(filho);
                                else
                                        avo->setLeft(filho);
                        }

                        filho->setPai(pai->getPai());
                        filho->setRight(pai);

                        pai->setPai(filho);
                        pai->setLeft(direitaDoFilho);

			if(direitaDoFilho != NULL)
                        	direitaDoFilho->setPai(pai);

			if(pai == raiz)
				raiz = filho;
                }
		void insercao_caso5(No *novo){

			No *avo = novo->avo();

			novo->getPai()->setCor(PRETA);
			avo->setCor(VERMELHA);

			if ((novo == novo->getPai()->getLeft()) && (novo->getPai() == avo->getLeft())) 
				rotacionar_direita(avo);
			
			else
				rotacionar_esquerda(avo);
			
		}

		void insercao_caso4(No *novo)
		{
        		No *avo = novo->avo();

        		if ((novo == novo->getPai()->getRight()) && (novo->getPai() == avo->getLeft())) {
                		rotacionar_esquerda(novo->getPai());
                		novo = novo->getLeft();
        		}
			else if ((novo == novo->getPai()->getLeft()) && (novo->getPai() == avo->getRight())) {
                		rotacionar_direita(novo->getPai());
                		novo = novo->getRight();
        		}
        		insercao_caso5(novo);
		}

		void insercao_caso3(No *novo){

			No *tio = novo->tio();
			No *avo = novo->avo();
 
			if ((tio != NULL) && (tio->getCor() == VERMELHA)) {
				novo->getPai()->setCor(PRETA);
				tio->setCor(PRETA);
				avo->setCor(VERMELHA);
				insercao_caso1(avo);
			}
			else
				insercao_caso4(novo);
			
		}

		void insercao_caso2(No *novo){
			if (novo->getPai()->getCor() == PRETA)
				return; /* Árvore ainda é válida */
			else
				insercao_caso3(novo);
		}

		void insercao_caso1(No *novo)
		{
			if (novo == raiz)
				novo->setCor(PRETA);
			else
				insercao_caso2(novo);
		}

	public:

		void insercao(int info){

			No *novo = new No;

                        novo->setInfo(info);
                        novo->setCor(VERMELHA);

			if(raiz == NULL)

				raiz = novo;

			else{

				No *lugarCerto = raiz;
				No *pai;

				while(lugarCerto != NULL){

					pai = lugarCerto;

                               		if(novo->getInfo() < lugarCerto->getInfo())
                                        	lugarCerto = lugarCerto->getLeft();
                                	else
                                        	lugarCerto = lugarCerto->getRight();
				}

				if(novo->getInfo() < pai->getInfo())
					pai->setLeft(novo);
				else
					pai->setRight(novo);

                                novo->setPai(pai);
			}

			insercao_caso1(novo);			
			
		}
		void printarArvore(No* raiz){

			int altura;

			for(altura = 1; raiz->getLeft() != NULL; altura++)
				raiz = raiz->getLeft();

			while(altura != 0){

				printf("%d, ", raiz->getInfo());

				if(raiz->getRight() != NULL)
					printarArvore(raiz->getRight());

				raiz = raiz->getPai();
				altura--;
			}
		}
		No* getRaiz(){
			return raiz;
		}
};
