using namespace std;

class No{

	private:
		int info;
		No *left;
		No *right;
		No *pai;
		bool cor;

	public:
		void setInfo(int info){
			this->info = info;
		}
		int getInfo(){
			return info;
		}
		void setLeft(No *left){
			this->left = left;
		}
		No *getLeft(){
			return left;
		}
		void setRight(No *right){
			this->right = right;
		}
		No *getRight(){
			return right;
		}
		void setPai(No *pai){
			this->pai = pai;
		}
		No *getPai(){
			return pai;
		}
		void setCor(bool cor){
			this->cor = cor;
		}
		bool getCor(){
			return cor;
		}
		No *avo(){
			if(pai == NULL)
				return NULL;
			return pai->getPai();
		}
		No *tio(){
			No *umAvo = avo();
			if(umAvo->left == pai)
				return umAvo->right;
			return umAvo->left;
		}
};
